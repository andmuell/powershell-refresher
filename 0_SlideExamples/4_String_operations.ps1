return

# Double quotes -> Variables work
"Hello $env:USERNAME"

# More complex expressions have to be wrapped in $()
"PowerShell Version $PSVersionTable.PSVersion"      # does not work
"PowerShell Version $($PSVersionTable.PSVersion)"   # does work

# Very complex expressions can be put into $(), as long as the expressions do not contain double quotes
"Today's date is $(Get-Date -Format 'dd.MM.yyyy')"

# Single quotes -> Variables do not work
'Hello $env:USERNAME'

# With @" and "@ a very long string spanning multiple rows can be written
@"
Just some simple string

I'm the user $env:USERNAME

Today's $(Get-Date)
"@

# The same can be done for @' and '@ Same effect as above


# If you want the follwing output 
# "$PSEdition value is $PSEdition"
# We need to escape the $ with the escape mark `
"`$PSEdition value is $PSEdition"


