# do not remove this line, instead execute the script step by step
return;

################################

# In this task you will learn how to set the exeution preference correctly
# and what the implications of the different preferences are.

# As reference, these are all the preference variables available.
# Execute them to see the default value:
$ErrorActionPreference
$WarningPreference
$VerbosePreference
$DebugPreference
$InformationPreference


# To demonstrate, we will use the ERROR Stream
function Start-ExampleFunction {

    # count the nr of executions of the block
    $executions = 0 

    1..5 | ForEach-Object {

        # increment variable on every iteration
        $executions++ 

        # Send an Message to the ERROR Stream
        # Remember "$_" is automatically populated by the current value of Foreach-Object
        Write-Error "This is Error Nr$_"
    }

    # Give back the value
    return $executions
}

## SilentlyContinue
$ErrorActionPreference = "SilentlyContinue"
Start-ExampleFunction

# Did the last command execute successfully?
$?

# -> Everything executed fine, cool :)


## Stop
$ErrorActionPreference = "Stop"
Start-ExampleFunction

# -> Error is shown, and execution of the script stops immediately!
# -> Nothing is returned
# -> Last command is not marked as successful


## Continue
$ErrorActionPreference = "Continue"
Start-ExampleFunction

# -> We get a lot of errors, but execution still continued.
# -> Useful if we don't really need to do something about an error
#    -> for example if we query 100 computers and some are unreachable.
#       in this case "SilentlyContinue" would also be an option.


## Inquire
$ErrorActionPreference = "Inquire"
Start-ExampleFunction

# -> You get asked what you want to do for every error that occures
# -> Output of [?]:
#    Y - Continue with only the next step of the operation.
#    A - Continue with all the steps of the operation.
#    H - Stop this command.
#    S - Pause the current pipeline and return to the command prompt. Type "exit" to resume the pipeline.



### Special parameters
# "Ignore" and "Suspend" are only supported as parameters on functions
# $ErrorActionPreference = "Ignore" does not work
# $ErrorActionPreference = "Suspend" does also not work


## Ignore
# But the following works:
Write-Error -Message "This will be ignored" -ErrorAction Ignore

# -> This does exact what you expect, it ignores the error message :)


## Suspend
## This is only supported in powershell workflows, so the demonstration will not work as you'll see:
Write-Error -Message "This is an error" -ErrorAction Suspend




######################################################################

#### To see all available options of an Enumeration use [enum]::GetValues():
[Enum]::GetValues([System.Management.Automation.ActionPreference])



