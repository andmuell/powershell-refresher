# Get-Childitem
cd 'C:\Program Files\LAPS'
Get-ChildItem -Recurse -Depth 2 | Select LastAccessTime,Name

Get-ChildItem -Recurse -Depth 2 | Select LastAccessTime,Name,Length,PSIsContainer


# Format-List / Select-Object
Get-PhysicalDisk
Get-PhysicalDisk | Format-List *
Get-PhysicalDisk | Select-Object FriendlyName,FirmwareVersion,MediaType,Size

# Format-Table
Get-ADUser -Filter {name -like "asc4*"} 
Get-ADUser -Filter {name -like "asc4*"} | Format-Table

