# To keep you from completing this task in 1s
return


# Task 1.1: See all available modules on your system
Get-Module -ListAvailable


# Task 1.2: Import the ActiveDirectory Module
Import-Module ActiveDirectory

# 'cd' is an alias for Set-Location
Set-Location "AD:\OU=BIOL-MICRO,OU=BIOL,OU=Hosting,DC=D,DC=ETHZ,DC=CH"

# Task 1.3: Examine the output
Get-ChildItem -Recurse -Properties "mail" | Where-Object objectClass -eq 'User' | Select-Object Name,Mail

# Task 1.4: What is the difference compared to:
Get-ADObject -SearchBase "OU=BIOL-MICRO,OU=BIOL,OU=Hosting,DC=D,DC=ETHZ,DC=CH" -Filter "*" -Properties Mail | Where-Object objectClass -eq 'User' | Select-Object Name,Mail

# Exactly the same!
