# 
return;

### Section 2: Formatting examples ###

## Task 2.0: Load AD information

# This will load the ad information for your logged in user
$User = Get-ADUser -Identity $env:USERNAME

# Display the default formatting:
$User

#####################


## Task 2.1: Format the output

# The formatting is default according to the amount of properties returned
# to verify this, we will select only *4* properties:
$User | Select-Object Enabled,Name,GivenName,Surname

# and with *>=5* properties, the formatting changes automatically
$User | Select-Object Enabled,Name,GivenName,Surname,UserPrincipalName

#####################

# We can also force PowerShell to use a certain formatting
# -> But this shows all properties that fit and skips the rest (may not be useful)
$User | Format-Table 

# So we can explicitly set the properties that we want to see:
# -> This behaves exactly like Select-Object
$User | Format-Table Enabled,Name,GivenName,Surname,UserPrincipalName

# ---- Note: The line above is the same as:
$User | Select-Object Enabled,Name,GivenName,Surname,UserPrincipalName | Format-Table

#### Conclusion -> in PowerShell the are multiple ways to the same result!


#####################

## Task 2.2: List formatting

# We can also do the same with Format-List as with Format-Table
$User | Format-List 
$User | Format-List Enabled,Name,GivenName,Surname,UserPrincipalName


#####################


## Task 2.3: Custom "Views" of objects
# We can also create our custom "views" for AD Users

# Note: This example has a bug in pwsh 6.2.3
$User = Get-ADUser -Identity $env:USERNAME -Properties *

# NOTE: If the above example does not work, use the workaround (PS 5 fallback):
$User = (powershell.exe -command "(Get-ADUser -Identity $env:USERNAME -Properties * | ConvertTo-Json)") | ConvertFrom-Json -AsHashtable

$User | Select-Object Enabled,Name,primarygroupid,Description,telephoneNumber







## For interested people:

#### Explanation for the PS 5 fallback:
# This runs powershell.exe with the command 'Get-ADUser -Identity $env:USERNAME -Properties *' and then pipes it to 'ConvertTo-Json'
# Example:
$json = Get-ADGroup "Domain Admins" | ConvertTo-Json
# This returns the object in JSON notation, which is then parsed again in the current session with 'ConvertFrom-Json'
# The Endresult is the same.

# Example:
$json | ConvertFrom-Json -AsHashtable

# We need to use -asHashTable, because it contains multiple keys with different casing ObjectGuid and ObjectGUID...


